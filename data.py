import pandas
import numpy as np

def blockify(x, n, char_to_ix = None, fill_char = '~'):
	'''
	  trim x into a string of n-characters
	'''
	if not char_to_ix is None:
		if not fill_char in char_to_ix:
			char_to_ix[fill_char] = max(char_to_ix.values()) + 1
		# remove letters not present in the alphabet
		x = filter(lambda c: c in char_to_ix, x)

	if len(x) > n:
		x = x[:n]
	elif len(x) < n:
		x = x + ''.join([fill_char] * (n - len(x)))
	return x

def encodex(x, char_to_ix):
	'''
	  map each character in string x to a corresponding index in the vocabulary
	'''
	_x = []
	for c in x:
		# skip symbols not found in the alphabet 
		if c in char_to_ix:
			_x.append(char_to_ix[c])
	return _x
	
def get_data(input_file = 'input.txt', n = 20):
	'''
		load data and preprocess it
	'''
	data = pandas.read_table(input_file, sep='\t', header = None, names = ['input', 'intent'])
	original = data['input'].tolist()

	chars = list(set(''.join(data['input'].tolist())))
	data_size, vocab_size = data.shape[0], len(chars)
	y_size = len(data['intent'].unique())

	print 'data has %d inputs strings, and %d unique characters.' % (data_size, vocab_size)
	char_to_ix = { ch:i for i,ch in enumerate(chars) }
	ix_to_char = { i:ch for i,ch in enumerate(chars) }

	for i in range(data.shape[0]):
		data['input'][i] = blockify(data['input'][i], n, char_to_ix)

	intent_to_iy = { ch:i for i,ch in enumerate(data['intent'].unique()) }
	iy_to_intent = { i:ch for i,ch in enumerate(data['intent'].unique()) }


	x = []
	y = []
	for i in range(data.shape[0]):
		xi = data['input'][i]
		yi = data['intent'][i]
		x.append(encodex(xi, char_to_ix))
		_y = np.zeros((y_size,))
		_y[intent_to_iy[yi]] = 1
		y.append(_y)


	return {'y_classes': y_size, 'x': np.array(x), 'y': np.array(y), 'string_size': n, 'original': original, 'char_to_ix': char_to_ix, 'ix_to_char': ix_to_char, 'intent_to_iy': intent_to_iy, 'iy_to_intent': iy_to_intent }

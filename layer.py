import numpy as np

'''
class DropOut:
	ratio = 0.3

	@classmethod
	def fprop(cls, x):
		mask = np.ones(x.shape, dtype = np.bool)
		mask[np.random.choice(xrange(len(x)), size = int(cls.ratio * len(x)), replace = False)] = False
		result = np.copy(x)
		result[mask] = 0
		return result 

	@classmethod
	def bprop(cls, x):
		# assumes that the activations in the layer preceding the dropout layer
		# have been overwritten by the dropout layer during fprop
		return x
'''

class DropOut:
	def __init__(self, ratio = 0.1):
		self.ratio = ratio
		self.reset()

	def reset(self):
		self.mask = None

	def fprop(self, x):
		if self.mask is None:
			self.mask = np.ones(x.shape)
			self.mask[np.random.choice(xrange(len(x)), size = int(self.ratio * len(x)), replace = False)] = 0
		result = np.copy(x)
		result *= self.mask
		result *= 1 / self.ratio
		return result 

	def bprop(self, x, grad):
		'''
		  Return gradient of the dropout layer
		'''
		result = np.copy(grad)
		result *= self.mask
		result *= 1 / self.ratio
		return result 


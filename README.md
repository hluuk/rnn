# RNN #

Modified version of the minimal character-level Vanilla RNN model originally written by Andrej Karpathy (@karpathy) https://gist.github.com/karpathy/d4dee566867f8291f086

Replaced tanh with ELU, repurposed from seq2seq to seq2class (i.e. useful for predicting the class of sequence). 

Testing indicated that the memory of this RNN extends to at least 200 characters.

BSD License
#!/usr/bin/python

"""
Modified version of the minimal character-level Vanilla RNN model originally written by Andrej Karpathy (@karpathy)
BSD License

  https://gist.github.com/karpathy/d4dee566867f8291f086

"""
import sys
import numpy as np
import re

from data import get_data, encodex, blockify
import pickle

import layer
import loss
import act


def rand_fill(shape, mode = 'uniform', mu = 0, sigma = 0.1):
	if 'uniform' in mode:
		return np.random.randn(shape[0], shape[1]) * 0.01 
	else:
		return np.random.normal(loc=mu, scale=sigma, size=shape)
		

class RNN:
	def __init__(self, data = None, dropout = 0):
		if type(data) is str:
			self.init(None, dropout)
			self.load(data)
		else:
			self.init(data, dropout)

	def init(self, data = None, dropout = 0):
		self.hidden_size = 100
		self.lossClass = loss.CrossEntropy
		self.dropout = layer.DropOut(dropout)
		if not data is None:
			self.data = data
			self.init_model()

	def init_model(self):
		# size of the alphabet
		self.input_size = len(self.data['char_to_ix'])

		# no. of intent classes
		self.output_size = self.data['y_classes']

		# no. of timesteps to evaluate is tmax + 1
		self.tmax = self.data['string_size'] - 1

		# init parameters
		self.model = {}
		self.model['Wxh'] = rand_fill((self.hidden_size, self.input_size), mode = 'normal') # input to hidden
		self.model['Whh'] = rand_fill((self.hidden_size, self.hidden_size), mode = 'normal') # hidden to hidden
		self.model['Why'] = rand_fill((self.output_size, self.hidden_size), mode = 'normal') # hidden to output
		self.model['bh'] = np.zeros((self.hidden_size, 1)) # hidden bias
		self.model['by'] = np.zeros((self.output_size, 1)) # output bias
		
	def save(self, prefix):
		'''
		  save the model
		'''
		with open(prefix + '.pickle', 'wb') as f:
			pickle.dump({'data': self.data, 'model': self.model}, f)

	def load(self, prefix):
		'''
		  load the model and the data, which the model is based on 
		'''
		prefix = re.sub(r'\.pickle', '', prefix)
		with open(prefix + '.pickle', 'rb') as f:
			session = pickle.load(f)
		self.data = session['data']
		# must initialize model topology
		self.init_model()
		self.model = session['model']

	def fprop(self, x):
		'''
		  forward pass 
		'''
		xs, hs, ys, ps = {}, {}, {}, {}

		# reset the initial hidden state
		hs[-1] = np.zeros((self.hidden_size,1))

		for t in xrange(self.tmax + 1):
			# encode in 1-of-k representation (one hot encoding)
			xs[t] = np.zeros((self.input_size, 1))
			# set the hot bit
			try:
				xs[t][x[t]] = 1
			except Exception as e:
				print "Error: len(x) %d, t %d out of bounds" % (len(x), t)
				print "Error: len(xs[t]) %d, index x[t] %d out of bounds" % (len(xs[t]), x[t])
				raise RuntimeError('Quitting: %s' % e)
			hs[t] = act.ELU.fprop(np.dot(self.model['Wxh'], xs[t]) + np.dot(self.model['Whh'], hs[t-1]) + self.model['bh']) # hidden state
			if self.dropout.ratio > 0:
				hs[t] = self.dropout.fprop(hs[t])
			# raw output
			ys[t] = np.dot(self.model['Why'], hs[t]) + self.model['by']
			# softmax probabilities for output
			ps[t] = np.exp(ys[t]) / np.sum(np.exp(ys[t]))  
		return [xs, hs, ys, ps]

	def bprop(self, xs, hs, ps, dy):
		'''
		  propagate gradient of loss function {dy} backwards in time

		  xs - input at timesteps 0..t
		  hs - hidden state at timesteps 0..t
		  ps - softmax probabilities of output at timesteps 0..t
		  dy - gradient of the loss function at timepoint t
		  
		'''
		dWxh, dWhh, dWhy = np.zeros_like(self.model['Wxh']), np.zeros_like(self.model['Whh']), np.zeros_like(self.model['Why'])
		dbh, dby = np.zeros_like(self.model['bh']), np.zeros_like(self.model['by'])

		# gradient of weights between hidden (h) and output layers (y)
		dWhy = np.dot(dy, hs[self.tmax].T)
		dby += dy

		# gradient of the hidden layer at tmax 
		dhnext = np.dot(self.model['Why'].T, dy) 

		# backprop
		for t in reversed(xrange(self.tmax + 1)):
			dh = dhnext
			if self.dropout.ratio > 0:
				# backprop through dropout
				dh = self.dropout.bprop(hs[t], dh)
			# backprop through activation function
			dhraw = act.ELU.bprop(hs[t], dh)
			dbh += dhraw
			dWxh += np.dot(dhraw, xs[t].T)
			dWhh += np.dot(dhraw, hs[t-1].T)
			# backprop the gradient one step back in time (i.e. from hidden(t) to hidden(t-1))
			dhnext = np.dot(self.model['Whh'].T, dhraw)

		return [dWxh, dWhh, dWhy, dbh, dby]

	def next(self, inputs, targets):
		"""
		perform a forward and backward pass
		inputs,targets are both list of integers.
		returns the loss and gradients on model parameters
		"""
		xs, hs, ys, ps = self.fprop(inputs)
		yloss, dy = self.lossClass.get(ps[self.tmax], targets)
		dWxh, dWhh, dWhy, dbh, dby = self.bprop(xs, hs, ps, dy)

		return yloss, dWxh, dWhh, dWhy, dbh, dby

	def test(self, x, pretty_print = False):
		'''
		  run x through the pretrained model.
		  Note! if the model was pre-trained with dropout, need to post-train
		'''
		if type(x) is str or type(x) is unicode:
			x = blockify(x, self.tmax + 1, self.data['char_to_ix'])
			x = encodex(x, self.data['char_to_ix'])

		_, _, _, ps = self.fprop(x)

		ps = ps[self.tmax]
		winner = np.argmax(ps)

		if pretty_print:
			result = rnn.pretty_print_p(ps)
			print result
		return (self.data['iy_to_intent'][winner], ps)

def pretty_print_p(p, target = None):
	winner = np.argmax(p)
	result = []
	for i in range(len(p)):
		_result = ''
		if p[i] == winner:
			_result += '*'
		_result += '%s' % (data['iy_to_intent'][i],)
		if p[i] == winner:
			_result += '*'
		_result += ' (%.2f)' % (p[i],)
		result.append(_result)
	result = 'predict: ' + ', '.join(result)
	if not target is None:
		correct = ' '.join(['%s (%s)' % (data['iy_to_intent'][i], target[i]) for i in range(len(target))]) 
		result += '\ntarget: ' + correct
	return result 

def accuracy(net, data):
	'''
	  evaluate the accuracy of {net} on the {data}
	'''
	acc_result = {}
	index = {
	  'full': range(data['x'].shape[0]),
	  'balanced': balanced_index(data['y'], data['y'].shape[1], data['x'].shape[0] / data['y'].shape[1])
	}

	for key in index:
		acc_result[key] = {}
		tp = 0
		for i in index[key]:
			result = net.fprop(data['x'][i,:])
			# get output from the final time step
			p = result[-1]
			p = p[max(p.keys())]
			tp += (np.argmax(p) == np.argmax(data['y'][i,:]))
		acc = tp / float(len(index[key]))
		acc_result[key]['acc'] = acc

	random_acc = 1.0 / data['y'].shape[1]
	return [acc_result['balanced']['acc'], 'Accuracy on training set with balanced classes: %.2f%% (random %.2f%%)' % (acc_result['balanced']['acc'] * 100, random_acc * 100),
	'Accuracy on full training set: %.2f%%' % (acc_result['full']['acc'] * 100)]

def sample(net, inputs, target):
	""" 
	sample {s} is a sequence of integers corresponding to an encoded string
	print ps
	h is memory state
	"""
	_, _, _, p = net.fprop(inputs)
	# we are interested only in the final state
	p = p[np.max(p.keys())]

	yloss, _ = net.lossClass.get(p, target)
	result = pretty_print_p(p, target)
	return result + ('\nLoss: %.2f' % yloss)

def print_norm(logger, title, Wxh, Whh, Why, dbh, dby):
	if 'gradient' in title:
		norm = 'gradient norms: %.2f (dWxh), %.2f (dWhh), %.2f (dWhy), %.2f (dbh), %.2f (dby)'
	else:
		norm = 'parameter norms: %.2f (Wxh), %.2f (Whh), %.2f (Why), %.2f (bh), %.2f (by)'
		
	norm = norm % (np.linalg.norm(Wxh), np.linalg.norm(Whh), np.linalg.norm(Why), np.linalg.norm(dbh), np.linalg.norm(dby))
	logger.write(norm + '\n')
	print norm 

def balanced_index(y, batch_size, n):
	'''
	  y is a target matrix, with each row corresponding to a sample
	  generate random index with balanced classes in each batch
	  n - number of index batches to generate
	'''
	class_index = [np.where(y[:,i] > 0.8)[0] for i in range(y.shape[1])]
	examples_per_batch = int(np.ceil(float(batch_size) / len(class_index)))
	index = []
	for i in range(n):
		batch_index = []
		for ci in class_index:
			# choose an equal number of random examples from each class 
			if len(ci) < examples_per_batch:
				batch_index += np.random.choice(ci, examples_per_batch, replace = True).tolist()
			else:
				batch_index += np.random.choice(ci, examples_per_batch, replace = False).tolist()
		# randomize the order of items within the batch and crop it to batch_size
		np.random.shuffle(batch_index)
		index += batch_index[:batch_size]
	return np.array(index)

def optimize(net, n = 10000, batch_size = 32, logger = None):
	# optimizer parameters
	learning_rate = 5*1e-3 # suitable for ELU with batch_size 32-42

	# memory variables for Adagrad
	mWxh, mWhh, mWhy = np.zeros_like(net.model['Wxh']), np.zeros_like(net.model['Whh']), np.zeros_like(net.model['Why'])
	mbh, mby = np.zeros_like(net.model['bh']), np.zeros_like(net.model['by'])

	dWxh, dWhh, dWhy = np.zeros_like(mWxh), np.zeros_like(mWhh), np.zeros_like(mWhy)
	dbh, dby = np.zeros_like(mbh), np.zeros_like(mby)

	nsample = net.data['x'].shape[0]

	# generate index for n random batches
	random_index = balanced_index(net.data['y'], batch_size, n)

	best_accuracy = 0
	start_loss = 0

	# sample counter for printing purposes only
	si = 0

	# scan through rows of data['x']
	it = 0
	while it < n:
		batch_loss = 0
		dWxh.fill(0), dWhh.fill(0), dWhy.fill(0)
		dbh.fill(0), dby.fill(0)

		batch = random_index[(it*batch_size):min(len(random_index), ((it+1)*batch_size))]

		# forward seq_length characters through the net and fetch gradient
		for b in batch:
			x = net.data['x'][b,:]
			y = net.data['y'][b,:]
			_loss, _dWxh, _dWhh, _dWhy, _dbh, _dby = net.next(x, y)
			# clip to mitigate exploding gradients
			for dparam in [_dWxh, _dWhh, _dWhy, _dbh, _dby]:
				np.clip(dparam, -5, 5, out=dparam)
			dWxh += _dWxh
			dWhh += _dWhh
			dWhy += _dWhy
			dbh += _dbh
			dby += _dby
			batch_loss += _loss

		if net.dropout.ratio > 0:
			# reset the dropout mask after each batch
			net.dropout.reset()

		if np.isnan(batch_loss):
			print "Model exploded at iteration %d, restarting training!" % it
			net.init_model()
			it = 0
			best_loss = 1e6
			start_loss = 0
			continue

		if it % 100 == 0: 
			if it == 0:
				start_loss = float(batch_loss)
			print '----'
			progress = 'iter %d, loss: %f' % (it, batch_loss)
			print progress # print progress
			if not logger is None:
				logger.write(progress + '\n')
				print_norm(logger, 'parameter', net.model['Wxh'], net.model['Whh'], net.model['Why'], net.model['bh'], net.model['by'])
				print_norm(logger, 'gradient', dWxh, dWhh, dWhy, dbh, dby)

			# select an example
#			print "[DEBUG] data['x'][ri,:] %s" % data['x'][ri,:]
			s = si % nsample
			intent = sample(net, net.data['x'][s,:], net.data['y'][s,:])
			txt = 'input: %s\n%s\n' % (data['original'][s], intent)
			training_accuracy, acc_bal, acc_full = accuracy(net, data)
			txt += acc_bal + '\n' + acc_full 
			print '%s\n' % (txt, )
			if not logger is None:
				logger.write(txt + '\n')
			si += 1 
			if True: #batch_loss < 0.5*start_loss:
				net.save('model_iter%d_loss%.2f_acc%.2f' % (it, batch_loss, training_accuracy))

		if batch_loss < 0.5*start_loss and training_accuracy > best_accuracy:
			net.save('model_iter%d_loss%.2f_acc%.2f' % (it, batch_loss, training_accuracy))
			best_accuracy = training_accuracy

		if training_accuracy == 1:
			print "Model converged at iteration %d (perfect training accuracy)!" % it
			break

		# clip to mitigate exploding gradients
		for dparam in [dWxh, dWhh, dWhy, dbh, dby]:
			np.clip(dparam, -5, 5, out=dparam)

		# perform parameter update with Adagrad
		for param, dparam, mem in zip([net.model['Wxh'], net.model['Whh'], net.model['Why'], net.model['bh'], net.model['by']], 
									  [dWxh, dWhh, dWhy, dbh, dby], 
									  [mWxh, mWhh, mWhy, mbh, mby]):
			mem += dparam * dparam
			param += -learning_rate * dparam / np.sqrt(mem + 1e-8) # adagrad update

		it += 1

if __name__ == "__main__":
	# number of timesteps to unfold input
	string_size = 20

	if len(sys.argv) == 2:
		print "rnn.py initialized with data = %s" % sys.argv[1]
		data = get_data(sys.argv[1], n = string_size)
	elif len(sys.argv) == 3:
		n = int(sys.argv[2])
		print "rnn.py initialized with data = %s and No. of steps (string size) = %d" % (sys.argv[1], n)
		data = get_data(sys.argv[1], n = n)
	else:
		print "rnn.py initialized with data = %s" % 'input.txt'
		data = get_data('input.txt', n = string_size)

	net = RNN(data)
	with open('error.txt', 'w', 1) as f:
		optimize(net, logger = f)
	net.save('model_final')



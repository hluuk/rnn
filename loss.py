import numpy as np

class CrossEntropy:
	'''
	  Cross entropy loss
	'''
	@classmethod
	def fprop(cls, y, target):
		'''
		  y is a column vector of softmax probabilities
		  target is the true probabilities
		'''
		return -np.log(y[np.argmax(target),0])

	@classmethod
	def bprop(cls, y, target):
		'''
		  y is a column vector of softmax probabilities
		  target is the true probabilities

		  Note! current implementation assumes that target contains only one non-zero probability
		'''
		dy = np.copy(y)
		# backprop into y. see http://cs231n.github.io/neural-networks-case-study/#grad if confused here
		dy[np.argmax(target)] -= np.max(target)
		return dy

	@classmethod
	def get(cls, y, target):
		return [cls.fprop(y, target), cls.bprop(y, target)]

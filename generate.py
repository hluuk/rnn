#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
	Generate test data where the first item of x corresponds to y
	and the rest is noise.

	Use this dataset to test how long is your RNN-s memory. 


	Inspired by:
	Bengio, Y., Simard, P., and Frasconi, P. Learning long-term
	dependencies with gradient descent is difficult. IEEE
	Transactions on Neural Networks, 5(2):157–166, March
	1994. ISSN 1045-9227. doi: 10.1109/72.279181.

'''
import sys
import string
import numpy as np

def generate(string_length):
	x = []
	y = []
	y_classes = 4
	class_size = 200
	noise_size = string_length - 1
	alphabet = [a for a in string.ascii_lowercase] * np.ceil(float(string_length) / len(string.ascii_lowercase))
	alphabet = np.array(alphabet)[:noise_size]

	for yclass in range(y_classes):
		y += [yclass] * class_size
		for i in range(class_size):
			np.random.shuffle(alphabet)
			x.append(str(yclass) + ''.join(alphabet))
		
	with open('memory_test%d.txt' % string_length, 'w') as f:
		f.write('\n'.join(['%s\t%s' % (x[i], y[i]) for i in range(len(x))]))



if __name__ == "__main__":
	if len(sys.argv) < 2:
		print "Usage: ./generate.py {string_length}"
	else:
		generate(int(sys.argv[1]))

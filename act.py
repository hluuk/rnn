import numpy as np

class ELU:
	'''
	  http://arxiv.org/pdf/1511.07289v5.pdf
	  https://www.reddit.com/r/MachineLearning/comments/3u6ppw/exponential_linear_units_yielded_the_best/
	'''
	alpha = 1.0

	@classmethod
	def fprop(cls, x):
		#result = (x>=0)*x + (x<0) * cls.alpha * (np.exp(x)-1.0)
		result = np.copy(x)
		index = x <= 0
		#result[index] = cls.alpha * (np.exp(x[index]) - 1.0)
		# if alpha = 1
		result[index] = np.exp(x[index]) - 1.0
		return result 

	@classmethod
	def bprop(cls, x, grad):
		'''
		  http://arxiv.org/pdf/1511.07289v5.pdf
		  https://www.reddit.com/r/MachineLearning/comments/3u6ppw/exponential_linear_units_yielded_the_best/

		  HL: added 4 aug. 2016
		'''
		#result = (x>=0) + (x<0) * (cls.fprop(x) + cls.alpha)
		result = np.ones(x.shape)
		index = x <= 0
		#result[index] = cls.fprop(x[index]) + cls.alpha
		# if alpha = 1
		result[index] = np.exp(x[index])
		# backpropagate {grad} by multiplying with the gradient of ELU at x
		return result * grad
